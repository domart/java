site_name: POO Java
site_url: https://domart.frama.io/java/
site_dir: public

docs_dir: docs

nav:
    - TP3 - Les collections : 'TP3.md'
    - TP4 - Les tests unitaires : 'TP4.md'


theme:
    name: material
    font: false
    language: fr
    logo: images/java.png
    favicon: images/java.png
    features:
        - navigation.instant
        - navigation.tabs
        - navigation.expand
        - navigation.top
        - navigation.tracking      # Permet de mettre à jour l'URL avec l'ancre courante.
        - toc.integrate
        - navigation.tabs.sticky   # Permet de toujours afficher la barre de nav en haut.
        #- header.autohide
        - content.code.annotate
        - content.code.copy        # Pour afficher le bouton "copier" dans les blocs de code.

    palette:
        # Light mode
        - media: "(prefers-color-scheme: light)"
          scheme: default
          primary: teal
          accent: green
          toggle:
              icon: material/weather-night  #toggle-switch-off-outline
              name: Passer l'affichage en mode sombre

        # Dark mode
        - media: "(prefers-color-scheme: dark)"
          scheme: slate
          primary: grey
          accent: teal
          toggle:
              icon: material/weather-sunny   #toggle-switch
              name: Passer l'affichage en mode clair


markdown_extensions:
    - meta
    - abbr
    - def_list                      # Les listes de définition.
    - attr_list                     # Un peu de CSS et des attributs HTML.
    - md_in_html                    # Pour mettre du markdown dans du HTML.
    - footnotes                     # Notes[^1] de bas de page.  [^1]: ma note.
    - pymdownx.caret:               # Passage ^^souligné^^ ou en ^exposant^.
          insert: True
    - pymdownx.mark                 # Passage ==surligné==.
    - pymdownx.tilde                # Passage ~~barré~~ ou en ~indice~.
    - pymdownx.snippets             # Inclusion de fichiers externe.
    - admonition                    # Blocs colorés  !!! info "ma remarque" 
    - pymdownx.details              # ... qui peuvent se plier/déplier
    - pymdownx.highlight:           # Coloration syntaxique du code
          linenums: None
          auto_title: true
          auto_title_map:
              "Python": "🐍 Script Python"
              "Python Console Session": "🐍 Console Python"
              "Text Only": "📋 Texte"
              "E-mail": "📥 Entrée"
              "Text Output": "📤 Sortie"
              "Java": "☕ Code Java"
    - pymdownx.inlinehilite         # pour  `#!python  <python en ligne>`
    - pymdownx.tasklist:            # Cases à cocher  - [ ]  et - [x]
          custom_checkbox: false    # ...avec cases d'origine
          clickable_checkbox: true  # ...et cliquables.
    - pymdownx.superfences:         # Imbrication de blocs.
        custom_fences:              # mermaid permet de faire des diagrammes.
          - name: mermaid
            class: mermaid
            format: !!python/name:pymdownx.superfences.fence_code_format
    - pymdownx.keys:                # Touches du clavier.  ++ctrl+d++
        separator: "\uff0b"
    - pymdownx.tabbed:
          alternate_style: true
    - pymdownx.betterem:            # Amélioration de la gestion de l'emphase (gras & italique).
          smart_enable: all
    - pymdownx.arithmatex:          # Formules en LaTeX dans le MD, converties en MathJax
          generic: true
    - toc:
          permalink: ⚓︎
          toc_depth: 4
    - pymdownx.emoji:               # Émojis  :boom:
          emoji_index: !!python/name:materialx.emoji.twemoji
          emoji_generator: !!python/name:materialx.emoji.to_svg

plugins:
    - search
    - awesome-pages   # Pour avoir accès à "awesome" (warning par exemple)
    - macros

extra_css:
    - css/monStyle.css    # style perso.