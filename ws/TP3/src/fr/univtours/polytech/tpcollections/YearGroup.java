package fr.univtours.polytech.tpcollections;

import java.util.Collections;
import java.util.List;

public class YearGroup {

	private Integer year;

	private List<Student> students;

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public List<Student> getStudents() {
		return students;
	}

	public void setStudents(List<Student> students) {
		this.students = students;
	}

	public Double computeGroupMean() {
		Double mean = 0D;

		for (Student student : this.students) {
			mean += student.computeMean();
		}

		return mean / this.students.size();
	}

	public Double computeSubjectMean(Subject subject) {
		Double mean = 0D;

		for (Student student : this.students) {
			mean += student.getNotesList().get(subject).computeMean();
		}

		return mean / this.students.size();
	}

	public List<Student> getGroupRanking() {
		Collections.sort(this.students, (a, b) -> b.computeMean().compareTo(a.computeMean()));
		return this.students;
	}

	public Double getBestNote(Subject subject) {
		Collections.sort(this.students, (a, b) -> a.getNotesList().get(subject).computeMean()
				.compareTo(b.getNotesList().get(subject).computeMean()));
		return this.students.get(this.students.size() - 1).getNotesList().get(subject).computeMean();
	}

	public Double getWorseNote(Subject subject) {
		Collections.sort(this.students, (a, b) -> a.getNotesList().get(subject).computeMean()
				.compareTo(b.getNotesList().get(subject).computeMean()));
		return this.students.get(0).getNotesList().get(subject).computeMean();
	}
}
