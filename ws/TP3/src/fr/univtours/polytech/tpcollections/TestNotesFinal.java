package fr.univtours.polytech.tpcollections;

import java.util.ArrayList;
import java.util.List;

public class TestNotesFinal {

	public static void main(String[] args) {
		// Création de deux matières :
		Subject info = new Subject("Informatique", 1D);
		Subject maths = new Subject("Mathématiques", 2D);

		// Création de 3 étudiants :
		Student alice = new Student("Alice");
		Student bob = new Student("Bob");
		Student charlie = new Student("Charlie");

		List<Student> students = new ArrayList<Student>();
		students.add(alice);
		students.add(bob);
		students.add(charlie);

		// Création de la promotion :
		YearGroup yearGroup = new YearGroup();
		yearGroup.setYear(2022);
		yearGroup.setStudents(students);

		// Alice a 14 en info et 20 en maths.
		Notes aliceEnInfo = new Notes();
		aliceEnInfo.addNote(14D);
		alice.getNotesList().put(info, aliceEnInfo);
		Notes aliceEnMaths = new Notes();
		aliceEnMaths.addNote(20D);
		alice.getNotesList().put(maths, aliceEnMaths);

		// Bob a 16 en info et 10 en maths.
		Notes bobEnInfo = new Notes();
		bobEnInfo.addNote(16D);
		bob.getNotesList().put(info, bobEnInfo);
		Notes bobEnMaths = new Notes();
		bobEnMaths.addNote(10D);
		bob.getNotesList().put(maths, bobEnMaths);

		// Charlie a 15 en info et 15 en maths.
		Notes charlieEnInfo = new Notes();
		charlieEnInfo.addNote(15D);
		charlie.getNotesList().put(info, charlieEnInfo);
		Notes charlieEnMaths = new Notes();
		charlieEnMaths.addNote(15D);
		charlie.getNotesList().put(maths, charlieEnMaths);

		System.out.println("Classement :");
		System.out.println(yearGroup.getGroupRanking());

		System.out.println("Moyenne générale :");
		System.out.println(yearGroup.computeGroupMean());
		System.out.println("Moyenne en maths :");
		System.out.println(yearGroup.computeSubjectMean(maths));

		System.out.println("Meilleure note en maths :");
		System.out.println(yearGroup.getBestNote(maths));
		System.out.println("Moins bonne note en maths :");
		System.out.println(yearGroup.getWorseNote(maths));
	}
}
