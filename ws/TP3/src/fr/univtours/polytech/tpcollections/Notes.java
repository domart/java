package fr.univtours.polytech.tpcollections;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe modélisant une liste de notes.
 *
 */
public class Notes {

	private List<Double> notesList;

	public Notes() {
		this.notesList = new ArrayList<Double>();
	}

	public List<Double> getNotesList() {
		return notesList;
	}

	public void addNote(Double note) {
		this.notesList.add(note);
	}

	public Double computeMean() {
		// S'il n'y a pas de note ...
		if (this.notesList.size() == 0) {
			// ... on lève une exception.
			throw new ArithmeticException();
		}

		Double mean = 0D;

		for (Double note : this.notesList) {
			mean += note;
		}

		return mean / this.notesList.size();
	}
}
