package fr.univtours.polytech.tpcollections;

public class TestNotes {

	public static void main(String[] args) {
		Notes aliceEnInfo = new Notes();
		aliceEnInfo.addNote(10D);
		aliceEnInfo.addNote(12D);
		aliceEnInfo.addNote(20D);
		
		System.out.println(aliceEnInfo.computeMean());

		Subject maths = new Subject("Mathématiques", 2.5D);
		Subject info = new Subject("Informatique", 1.5D);
		
		Student alice = new Student("Alice");
		
		alice.getNotesList().put(maths, aliceEnInfo);
		
		// Création de ses notes :
		Notes aliceEnMaths = new Notes();
		aliceEnMaths.addNote(18D);
		aliceEnMaths.addNote(20D);
		// Ajout de ces 2 notes :
		alice.getNotesList().put(maths, aliceEnMaths);

		alice.getNotesList().put(info, aliceEnInfo);
		
		System.out.println(alice.computeMean());
	}
}
