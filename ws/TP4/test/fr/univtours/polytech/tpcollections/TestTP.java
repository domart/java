package fr.univtours.polytech.tpcollections;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class TestTP {

	private Student alice;
	private Subject maths;
	private Subject info;

	@Before
	public void avant() {
		Notes aliceEnInfo = new Notes();
		aliceEnInfo.addNote(10D);
		aliceEnInfo.addNote(12D);
		aliceEnInfo.addNote(20D);

		this.maths = new Subject("Mathématiques", 2.5D);
		this.info = new Subject("Informatique", 1.5D);

		this.alice = new Student("Alice");

		alice.getNotesList().put(maths, aliceEnInfo);

		// Création de ses notes :
		Notes aliceEnMaths = new Notes();
		aliceEnMaths.addNote(18D);
		aliceEnMaths.addNote(20D);
		// Ajout de ces 2 notes :
		alice.getNotesList().put(maths, aliceEnMaths);

		alice.getNotesList().put(info, aliceEnInfo);
	}

	@Test
	public void testInsertionNotes() {
		assertEquals(this.alice.getNotesList().get(this.maths).getNotesList().size(), 2);
		assertEquals(this.alice.getNotesList().get(this.info).getNotesList().size(), 3);
	}

	@Test
	public void testComputeMean() {
		Notes notes = new Notes();
		notes.addNote(15D);
		notes.addNote(16D);
		//

		assertEquals(notes.computeMean(), Double.valueOf(15.5D));
	}

}
