package fr.univtours.polytech.tpcollections;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class TestYearGroup {

	private YearGroup yearGroup;
	private Subject info;
	private Subject maths;
	private Student alice;
	private Student bob;
	private Student charlie;

	@Before
	public void setUp() {
		// Création de deux matières :
		this.info = new Subject("Informatique", 1D);
		this.maths = new Subject("Mathématiques", 2D);

		// Création de 3 étudiants :
		this.alice = new Student("Alice");
		this.bob = new Student("Bob");
		this.charlie = new Student("Charlie");

		List<Student> students = new ArrayList<Student>();
		students.add(alice);
		students.add(bob);
		students.add(charlie);

		// Création de la promotion :
		this.yearGroup = new YearGroup();
		yearGroup.setYear(2022);
		yearGroup.setStudents(students);

		// Alice a 14 en info et 20 en maths.
		Notes aliceEnInfo = new Notes();
		aliceEnInfo.addNote(14D);
		alice.getNotesList().put(info, aliceEnInfo);
		Notes aliceEnMaths = new Notes();
		aliceEnMaths.addNote(20D);
		alice.getNotesList().put(maths, aliceEnMaths);

		// Bob a 16 en info et 10 en maths.
		Notes bobEnInfo = new Notes();
		bobEnInfo.addNote(16D);
		bob.getNotesList().put(info, bobEnInfo);
		Notes bobEnMaths = new Notes();
		bobEnMaths.addNote(10D);
		bob.getNotesList().put(maths, bobEnMaths);

		// Charlie a 15 en info et 15 en maths.
		Notes charlieEnInfo = new Notes();
		charlieEnInfo.addNote(15D);
		charlie.getNotesList().put(info, charlieEnInfo);
		Notes charlieEnMaths = new Notes();
		charlieEnMaths.addNote(15D);
		charlie.getNotesList().put(maths, charlieEnMaths);
	}

	@Test
	public void testComputeGroupMean() {
		assertEquals(Double.valueOf(15D), this.yearGroup.computeGroupMean());
	}

	@Test
	public void testComputeSubjectMean() {
		assertEquals(Double.valueOf(15D), this.yearGroup.computeSubjectMean(this.maths));
		assertEquals(Double.valueOf(15D), this.yearGroup.computeSubjectMean(this.info));
	}

	@Test
	public void testGetGroupRanking_premier() {
		assertEquals(this.alice, this.yearGroup.getGroupRanking().get(0));
	}

	@Test
	public void testGetGroupRanking_dernier() {
		int nbOfStudents = this.yearGroup.getStudents().size();
		assertEquals(this.bob, this.yearGroup.getGroupRanking().get(nbOfStudents - 1));
	}

	@Test
	public void testComputeGetBestNote() {
		assertEquals(Double.valueOf(20D), this.yearGroup.getBestNote(this.maths));
		assertEquals(Double.valueOf(16D), this.yearGroup.getBestNote(this.info));
	}

	@Test
	public void testComputeGetWorseNote() {
		assertEquals(Double.valueOf(10D), this.yearGroup.getWorseNote(this.maths));
		assertEquals(Double.valueOf(14D), this.yearGroup.getWorseNote(this.info));
	}
}