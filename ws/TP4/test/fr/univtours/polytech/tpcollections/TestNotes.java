package fr.univtours.polytech.tpcollections;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;

import org.junit.Before;
import org.junit.Test;

public class TestNotes {

	private Notes notes;

	@Before
	public void setUp() {
		this.notes = new Notes();
		this.notes.addNote(15D);
		this.notes.addNote(16D);
	}

	@Test
	public void testAddNote() {
		assertEquals(2, this.notes.getNotesList().size());
	}

	@Test
	public void testComputeMean() {
		assertEquals(Double.valueOf(15.5D), this.notes.computeMean());
	}

	@Test
	public void testComputeMean_pasDeNote_throwsArithmeticException() {
		this.notes = new Notes();
		assertThrows(ArithmeticException.class, () -> this.notes.computeMean());
	}
}
